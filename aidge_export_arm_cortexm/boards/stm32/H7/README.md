# Aidge export for stm32h7

To compile the export and generate the binary
```
make
```

This command generates a `.elf` file which can be used to flash the program on board.