# Aidge Export for ARM CortexM systems

This plugin is to add in your `Aidge` environment to create exports for ARM CortexM systems.

## Installation

### From Source

To install the export manager from the gitlab repository, 
run these commands in the Python environment where aidge is already installed.

```
git clone https://gitlab.eclipse.org/eclipse/aidge/aidge_export_arm_cortexm.git
cd aidge_export_arm_cortexm
pip install .
```

## License

Aidge has a Eclipse Public License 2.0, as found in the [LICENSE](LICENSE).